from bson import ObjectId
from pymongo import MongoClient
import pandas as pd


def get_mongo_connection(db_name):
    client = MongoClient('mongodb://localhost:27017/')
    return client[db_name]


def get_mongo_collection(db, collection):
    return db[collection]


def parse_excell():
    file = 'new.xlsx'
    frame = pd.read_excel(pd.ExcelFile(file))
    db = get_mongo_connection('sport')

    collection_options = get_mongo_collection(db, 'options')
    collection_screens = get_mongo_collection(db, 'screens')
    collection_screen_config = get_mongo_collection(db, 'screenConfig')

    configs_list = []
    config = {
        "optionId": "5b83ea22f074b726bee41a29",
        "optionKey": "gym",
        "item": "BEAUTY",
        "baseEntity": "ORGANISATION",
        "direction": "SPORT",
        "screenDef": {
            "screenId": "5b855cedf074b7410cd59325",
            "type": "AREA",
            "key": "gym"
        },
        "_class": "com.heapix.sport.persistence.model.ScreenConfigEntity"
    }
    screen_definition = {}
    for value in frame.values:
        config['_id'] = ObjectId()
        screen_key = value[0].upper()
        option_key = value[1]

        config['optionKey'] = option_key

        screen_definition['key'] = screen_key

        cursor_options = find_options_by_direction_and_containing_key(collection_options, 'BEAUTY', option_key)
        cursor_screens = find_screens_by_key(collection_screens, screen_key)

        existing_screens = []
        for elem in cursor_screens:
            existing_screens.append(elem)

        existing_options = []
        for elem in cursor_options:
            existing_options.append(elem)

        if existing_screens:
            screen = existing_screens.pop()
            screen_definition['_id'] = screen['_id']

        if existing_options:
            option = existing_options.pop()
            config['optionId'] = option['_id']
            config['direction'] = option['direction']

        config['screenDef'] = screen_definition
        one_type_only = value[2]
        configs_list.append(config)
        collection_screen_config.insert_one(config)


def find_options_by_direction_and_containing_key(collection_options, direction, containing_key):
    return collection_options.find(
        {
            "direction": direction,
            '$or': [{"key": containing_key}]})


def find_screens_by_key(collection, key):
    return collection.find(
        {
            '$or': [{"type": key}]})


if __name__ == '__main__':
    parse_excell()

option = {
    "_id": ObjectId("5dc2bba3fab2e521d49a8073"),
    "optionId": "5b83ea22f074b726bee41a29",
    "optionKey": "gym",
    "item": "AREA",
    "baseEntity": "ORGANISATION",
    "direction": "SPORT",
    "screenDef": {
        "screenId": "5b855cedf074b7410cd59325",
        "type": "AREA",
        "key": "gym"
    },
    "_class": "com.heapix.sport.persistence.model.ScreenConfigEntity"
}
